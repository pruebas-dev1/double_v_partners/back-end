<?php

namespace App\Repositories;

use App\Models\Ticket;

class TicketRepository extends BaseRepository{

  public function __construct(Ticket $modelo){
    parent::__construct($modelo);
  }

  public function getList( $request = [], $with = [], $select = ['*'] ){
    $data = $this->model->select($select)->with($with)
    ->where(function ($query) use ($request) {
      if( !empty($request['searchQuery']) ){
        $query->orWhereHas('user', function ($x) use ($request) {
          $x->where('name', 'like', '%'.$request['searchQuery'].'%');
        });
      }
    });

    if( !empty($request['paginate']) ){
      $data = $data->paginate($request['perPage'] ?? 10);
    }else{
      $data = $data->get();
    }

      return $data;
  }

  public function store( $aRequest ){
    if( !empty($aRequest['id']) && $aRequest['id'] != 'null' ){
      $oData = $this->model->find($aRequest['id']);
    }else{
      $oData = $this->model::newModelInstance();
    }
    foreach( $aRequest as $key => $value ){
      $oData[$key] = $aRequest[$key];
    }
    $oData->save();
    return $oData;
  }

}
