<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
// Repositories
use App\Repositories\TicketRepository;

class TicketsController extends Controller{

  private $oTicketRepository;
  public function __construct(TicketRepository $TicketRepository = null) {
    $this->oTicketRepository = $TicketRepository;
  }

  /**
   * Display a listing of the resource.
   */
  public function index(){
      //
  }

  public function list(Request $Request){
    $oDataTicket = $this->oTicketRepository->getList(
      $Request->all(), ['user']
    );
    // foreach( $variable as $key => $value ){
    //   # code...
    // }

    return [
      'tickets'     => $oDataTicket,
      'lastPage'    => (property_exists($oDataTicket, 'lastPage')) ? $oDataTicket->lastPage() : 0,
      'totalData'   => (property_exists($oDataTicket, 'total')) ? $oDataTicket->total() : $oDataTicket->count(),
      'totalPage'   => (property_exists($oDataTicket, 'perPage')) ? $oDataTicket->perPage() : 0,
      'currentPage' => (property_exists($oDataTicket, 'currentPage')) ? $oDataTicket->currentPage() : 0,
    ];
  }

  
  public function store( Request $Request){
    $aReturn = ['code' => 200];
    try {
      DB::beginTransaction();
      $data = $this->oTicketRepository->store($Request->all());
      DB::commit();

      $sMessage = 'agregado';
      if( !empty($Request['id']) ){
        $sMessage = 'modificado';
      }

      $aReturn['message'] = 'Registro '.$sMessage.' correctamente';
      $aReturn['data'] = $data;
  }catch( \Exception $e ){
    DB::rollBack();
    $aReturn['code'] = 500;
    $aReturn['message'] = $e->getMessage();
  }
  return response()->json($aReturn, $aReturn['code']);
  }

  /**
   * Remove the specified resource from storage.
   */
  public function delete(string $ticketId){
    $aReturn = ['code' => 200];
    try {
      DB::beginTransaction();
      $oTicket = $this->oTicketRepository->find($ticketId);
      if( $oTicket ){
        $oTicket->delete();
        $sMessage = 'Registro eliminado correctamente';
      } else {
        $sMessage = 'El registro no existe';
      }
      DB::commit();
      $aReturn['message'] = $sMessage;
    }catch( \Exception $e ){
      DB::rollBack();
      $aReturn['message'] = $e->getMessage();
    }
    return response()->json($aReturn, $aReturn['code']);
  }
}
