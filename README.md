# back-end



## Getting started

Debes contar con composer, contar con un servidor web local, recomendamos Laragon:
Viene con Apache como servidor web por defecto, proporciona una configuración preconfigurada para ejecutar aplicaciones PHP en tu máquina local.

Clonar repositorio y ejecutar el comando:
```
composer install
```

## Crea tu archivo .env
Crea el archivo en la base del proyecto, configura tu coneccion a base de datos en tu archivo .env
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:3jpSiH2N264xrqRyFjWXVkcyFoERJ2nT5FiOG9j1OpU=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=prueba_doublev_back_end
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailpit
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS="hello@example.com"
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_HOST=
PUSHER_PORT=443
PUSHER_SCHEME=https
PUSHER_APP_CLUSTER=mt1

VITE_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
VITE_PUSHER_HOST="${PUSHER_HOST}"
VITE_PUSHER_PORT="${PUSHER_PORT}"
VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"
VITE_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```

## Iniciando Laravel
Una vez configurada la DB crea una base de datos con el nombre configurado en tu archivo .env en el campo DB_DATABASE

Luego corre el siguiente comando
```
php artisan migrate:fresh --seed
```
Ahora iniciar laravel con el comando
```
php artisan serve
```

## Consumiendo el Rest Api
Importa en Postman el archivo:
```
prueba-doublev.postman_collection.json
```

Ahora configura en Postman como se muestra en la imagen:
``` Captura-Postman.PNG ```