<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\TicketsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('ticket-list', [TicketsController::class, 'list'])->name('api.ticket.list');
Route::put('ticket-new', [TicketsController::class, 'store'])->name('api.ticket.new');
Route::delete('ticket-delete/{id}', [TicketsController::class, 'delete'])->name('api.ticket.delete');
Route::put('ticket-edit', [TicketsController::class, 'store'])->name('api.ticket.edit');